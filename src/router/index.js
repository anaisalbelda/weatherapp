import Vue from 'vue'
import Router from 'vue-router'
import Vuetify from 'vuetify'
import Ciudad from '../components/Ciudad'
import Eventos from '../components/Eventos'
import Tiempo from '../components/Tiempo'
import 'vuetify/dist/vuetify.min.css'

Vue.use(Router);
Vue.use(Vuetify);

export default new Router({
  routes: [
    {
      path: '/:ciudad',
      name: 'Ciudad',
      component: Ciudad,
      props: true
    },
    {
      path: '/:ciudad/eventos',
      name: 'Eventos',
      component: Eventos,
      props: true
    },
    {
      path: '/:ciudad/tiempo',
      name: 'Tiempo',
      component: Tiempo,
      props: true
    }
  ],
  // Elimina el # del router
  mode: 'history'
})
